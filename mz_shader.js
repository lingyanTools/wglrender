var PARTICLE_VERTEX_SHADER=`
attribute vec4 a_Position;
attribute vec2 a_TexCoord;
varying vec2 v_TexCoord;

void main() {
  gl_Position = a_Position;
  v_TexCoord = a_TexCoord;
}`;

var PARTICLE_FRAG_SHADER=`
precision mediump float;
uniform sampler2D u_Sampler;
uniform float time;
varying vec2 v_TexCoord;

float noise(vec3 p){
    vec3 i = floor(p);
    vec4 a = dot(i, vec3(1., 57., 21.)) + vec4(0., 57., 21., 78.);
    vec3 f = cos((p-i)*acos(-1.))*(-.5)+.5;
    a = mix(sin(cos(a)*a),sin(cos(1.+a)*(1.+a)), f.x);
    a.xy = mix(a.xz, a.yw, f.y);
    return mix(a.x, a.y, f.z);
}

float sphere(vec3 p, vec4 spr){
    return length(spr.xyz-p) - spr.w;
}

float flame(vec3 p){
    float d = sphere(p*vec3(1.,.5,1.), vec4(.0,-1.,.0,1.));
    return d + (noise(p+vec3(.0,time*2.,.0)) + noise(p*3.)*.5)*.25*(p.y) ;
}

float scene(vec3 p){
    return min(100.-length(p) , abs(flame(p)) );
}

vec4 raymarch(vec3 org, vec3 dir){
    float d = 0.0, glow = 0.0, eps = 0.02;
    vec3  p = org;
    bool glowed = false;
   
    for(int i=0; i<64; i++)
    {
        d = scene(p) + eps;
        p += d * dir;
        if( d>eps )
        {
            if(flame(p) < .0)
                glowed=true;
            if(glowed)
                glow = float(i)/64.;
        }
    }
    return vec4(p,glow);
}

vec4 getRunningBorder() {
    float dis = time;//mod(time, 4.0);
    vec4 color = vec4(1.0, 0.0, 0.0, 1.0);
    float alpha1 = max(0.0, 0.01 - v_TexCoord.y) * max(0.0, dis - v_TexCoord.x);
    float alpha = max(0.0, v_TexCoord.x - 0.99) * max(0.0, dis - 1.0 - v_TexCoord.y);
    alpha = max(alpha1, alpha);
    alpha1 = max(0.0, v_TexCoord.y - 0.99) * max(0.0, dis - 3.0 + v_TexCoord.x);
    alpha = max(alpha, alpha1);
    alpha1 = max(0.0, 0.01 - v_TexCoord.x) * max(0.0, dis - 4.0 + v_TexCoord.y);
    alpha = max(alpha, alpha1);
    return color * alpha * 1000.0;
    // return vec4(0.0);
}

void main() {
    vec2 v = -1.5 + 3. * v_TexCoord;
   
    vec3 org = vec3(0., -2., 4.);
    vec3 dir = normalize(vec3(v.x*1.6, -v.y, -1.5));
   
    vec4 p = raymarch(org, dir);
    float glow = p.w;
   
    vec4 col = mix(vec4(1.,.5,.1,1.), vec4(0.1,.5,1.,1.), p.y*.02+.4);
    
    vec4 borderColor = getRunningBorder();
    col= mix(vec4(0.), col, pow(glow*2.,4.));
   
    gl_FragColor = mix(borderColor, col, col.a);
}`;

var FIRE_FRAG_SHADER = `
precision highp float;
varying vec2 v_TexCoord;
uniform float time;

float hash(vec2 p)  // replace this by something better
{
    p  = 50.0*fract( p*0.3183099 + vec2(0.71,0.113));
    return -1.0+2.0*fract( p.x*p.y*(p.x+p.y) );
}

float noise_value(vec2 p)
{
    vec2 pi = floor(p);
    vec2 pf = fract(p);

    vec2 w = pf * pf * (3.0 - 2.0 * pf);

    return mix(
                mix(hash(pi + vec2(0.0, 0.0)), hash(pi + vec2(1.0, 0.0)), w.x),
                mix(hash(pi + vec2(0.0, 1.0)), hash(pi + vec2(1.0, 1.0)), w.x),
                w.y
            );
}

vec2 grad( vec2 p )  // replace this anything that returns a random vector
{
    float hashV = hash(p);
    // simple random vectors
    return vec2(cos(hashV),sin(hashV));
}

float noise_perlin( vec2 p )
{
    vec2 i = floor( p );
    vec2 f = fract( p );

    vec2 u = f*f*(3.0-2.0*f); // feel free to replace by a quintic smoothstep instead

    return mix( mix( dot( grad( i+vec2(0,0) ), f-vec2(0.0,0.0) ), 
                     dot( grad( i+vec2(1,0) ), f-vec2(1.0,0.0) ), u.x),
                mix( dot( grad( i+vec2(0,1) ), f-vec2(0.0,1.0) ), 
                     dot( grad( i+vec2(1,1) ), f-vec2(1.0,1.0) ), u.x), u.y);
}

float fbm(vec2 uv)
{
    float f=0.0;
    uv=uv*8.0;
    mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
    f += 0.5000*noise_perlin( uv ); uv = m*uv;
    f += 0.2500*noise_perlin( uv ); uv = m*uv;
    f += 0.1250*noise_perlin( uv ); uv = m*uv;
    f += 0.0625*noise_perlin( uv ); uv = m*uv;
    f = 0.5 + 0.5 * f;
    f = smoothstep( 0.0, 1.0, f );
    return f;
}

void main() {
    vec2 uv = v_TexCoord;
    float flame = fbm(vec2(uv.x, uv.y-time/4.0));
    float c = 1.0 - 16.0 * pow(flame * uv.y, 2.0);
    float c1 = c * (1.0 - pow(uv.y, 4.0));
    c1=clamp(c1,0.,1.);
    vec3 col = vec3(1.5*c1, 1.5*c1*c1*c1, c1*c1*c1*c1*c1*c1);
    // vec4 color = vec4(0.1,.5,1.,1.0);
    gl_FragColor = vec4(col, c1);
    // gl_FragColor = vec4(flame);
}`;