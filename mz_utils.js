function initShaders(gl, vshader, fshader) {
  var program = createProgram(gl, vshader, fshader);
  if (!program) {
    console.log('Failed to create program');
    return false;
  }
  gl.useProgram(program);
  gl.program = program;
  return true;
}

function createProgram(gl, vshader, fshader) {
  var vertexShader = loadShader(gl, gl.VERTEX_SHADER, vshader);
  var fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fshader);
  if (!vertexShader || !fragmentShader) {
    return null;
  }
  var program = gl.createProgram();
  if (!program) {
    return null;
  }
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  var linked = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!linked) {
    var error = gl.getProgramInfoLog(program);
    console.log('Failed to link program: ' + error);
    gl.deleteProgram(program);
    gl.deleteShader(fragmentShader);
    gl.deleteShader(vertexShader);
    return null;
  }
  return program;
}

function loadShader(gl, type, source) {
  var shader = gl.createShader(type);
  if (shader == null) {
    console.log('unable to create shader');
    return null;
  }
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (!compiled) {
    var error = gl.getShaderInfoLog(shader);
    console.log('Failed to compile shader: ' + error);
    gl.deleteShader(shader);
    return null;
  }
  return shader;
}

function getWebGLContext(canvas, opt_debug) {
  var gl = WebGLUtils.setupWebGL(canvas);
  if (!gl) return null;
  return gl;
}

/**
 * 加载纹理
 * @param gl：webgl
 * @param shaderName：纹理在shader中的名字
 * @param imgPath ：纹理的本地路径，或者网络路径
 * @param texUnit ：纹理单元，支持0、1、2
 * @returns 
 */
function initTextures(gl,shaderName,imgPath,texUnit) {
    var texture = gl.createTexture(); 
    if (!texture) {
      console.log('Failed to create the texture');
      return false;
    }
    var u_Sampler = gl.getUniformLocation(gl.program, shaderName);
    if (!u_Sampler) {
      console.log('Failed to get the storage location of u_Sampler');
      return false;
    }
    var image = new Image();
    if (!image) {
      console.log('Failed to create the image object');
      return false;
    }
    image.onload = function(){ loadTexture(gl,texture, u_Sampler, image, texUnit); };
    image.src = imgPath;
    image.crossOrigin="*";
    return true;
  } 
  function loadTexture(gl, texture, u_Sampler, image, texUnit) {
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
    switch(texUnit)
    {
        case 0:
            gl.activeTexture(gl.TEXTURE0);
            break;
        case 1:
            gl.activeTexture(gl.TEXTURE1);
            break;
        case 2:
            gl.activeTexture(gl.TEXTURE2);
            break;
    }
    gl.bindTexture(gl.TEXTURE_2D, texture);   
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.uniform1i(u_Sampler, texUnit); 
  }
  var updateTime;
  var interval;
  var lastTime = new Date();
  var mainLoop;
  function setUpdate(fps,updateCallback)
  {
    interval = 1000/fps;
    mainLoop = updateCallback;
    tick();
  }

  function tick()
  {
    requestAnimationFrame(tick.bind(this));
    let now = new Date();
    let delta = now - lastTime;
    if(delta>=interval)
    {
        lastTime = now - (delta%interval);
        mainLoop();
    }
  }