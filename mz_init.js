var gl;
function main()
{
    var canvas = document.getElementById("wglCanvas");
    gl = getWebGLContext(canvas);
    if(!gl)
    {
        console.log("获取webgl渲染上下文失败");
        return;
    }
    particle_init();
    gl.clearColor(0.0,0.0,0.0,1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
}